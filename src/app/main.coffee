require.config {
   "baseUrl" : "js"
}

require [
   'require',
   'domReady',
   'i18n',
   'text',
   '_/context',
   '_/itemView',
   '_/layout'
], (require)->
   "use strict"

   # Change the Marionette render engine to Dust
   Backbone.Marionette.Renderer.render = (template, data) ->
      html = null
      dust.render template, data, (err, out) ->
         html = out
         return
      html

   $.ajax({ url: "modules.json"}).done (data) ->
      require.modules = data
      return

   require._loadModule = (name, callback) ->
      require [require.modules[name]], () ->
         callback()
         return
      return

   require.loadModule = (name, callback) ->
      if require.modules
         require._loadModule(name, callback)
      else $.ajax({ url: "modules.json"}).done (data) ->
         require.modules = data
         require._loadModule(name, callback)
      return

   require.loadModule 'core', () ->
      require ['core/application'], () ->


   return