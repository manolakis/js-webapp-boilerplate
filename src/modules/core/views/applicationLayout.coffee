define [
   '_/layout'
], (Layout) ->
   'use strict'

   class ApplicationLayout extends Layout
      el: '#app-container'
      template: 'core-application-layout'

      regions:
         menu: '#menu'
         content: '#content'

      initialize: (options) ->
         Backbone.Geppetto.bindContext
            view: @
            context: options.context
         return

