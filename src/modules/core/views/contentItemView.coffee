define [
   '_/itemView'
], (ItemView) ->
   'use strict'
   class ContentItemView extends ItemView
      template: 'core-content-item-view'