define [
   '_/itemView'
], (ItemView) ->
   'use strict'
   class MenuItemView extends ItemView
      template: 'core-menu-item-view'