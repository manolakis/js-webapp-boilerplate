define [
   'core/views/applicationLayout',
   'core/contexts/applicationContext',
   'core/views/contentItemView',
   'core/views/menuItemView'
], (ApplicationLayout, ApplicationContext, ContentItemView, MenuItemView) ->
   application = new Backbone.Marionette.Application()

   application.addInitializer (options) ->
      @applicationLayout = new ApplicationLayout { context: ApplicationContext }
      @applicationLayout.render()

      @applicationLayout.menu.show(new MenuItemView())
      @applicationLayout.content.show(new ContentItemView())
      return

   application.start()