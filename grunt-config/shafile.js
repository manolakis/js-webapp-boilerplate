(function() {
    'use strict';

    exports.config = function(options) {

        var config = {
            'development-libs': {
                algorithm: 'sha1',
                env: 'template.development.variables.libs',
                dest: 'dist/js/libs-{sha}.js',
                src: 'temp/libs.js'
            },
            'development-main': {
                algorithm: 'sha1',
                env: 'template.development.variables.main',
                dest: 'dist/js/main-{sha}.js',
                src: 'temp/build/main.js'
            },
            'development-css' : {
                algorithm: 'sha1',
                env: 'template.development.variables.css',
                dest: 'dist/css/main-{sha}.css',
                src: 'temp/main.css'
            },
            'production-libs': {
                algorithm: 'sha1',
                env: 'template.production.variables.libs',
                dest: 'dist/js/libs-{sha}.js',
                src: 'temp/libs.js'
            },
            'production-main': {
                algorithm: 'sha1',
                env: 'template.production.variables.main',
                dest: 'dist/js/main-{sha}.js',
                src: 'temp/build/main.js'
            },
            'production-css' : {
                algorithm: 'sha1',
                env: 'template.production.variables.css',
                dest: 'dist/css/main-{sha}.css',
                src: 'temp/main.css'
            }
        };

        return config;
    };

}());
