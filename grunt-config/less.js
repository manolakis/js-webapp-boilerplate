(function() {
    'use strict';

    exports.config = function(options) {

        var files = {
                "temp/main.css": "temp/styles/less/main.less"
            },
            config = {
                development: {
                    options: {},
                    files: files
                },
                production: {
                    options: {
                        copress: true,
                        yuicompress: true
                    },
                    files: files
                }
            };

        return config;
    };

}());