(function() {
    'use strict';

    exports.config = function(options) {

        var config = {
            options: {
                bare: false
            },
            main: {
                expand: true,
                flatten: false,
                cwd: 'temp',
                src: ['**/*.coffee'],
                dest: 'temp',
                ext: '.js'
            }
        };

        return config;
    };

}());