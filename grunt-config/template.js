(function() {
    'use strict';

    exports.config = function(options) {

        var config = {
            development: {
                src: 'src/templates/index.template.us',
                dest: 'dist/index.html',
                variables: {
                    gae: false,
                    min: false,
                    opts: options
                }
            },
            production: {
                src: 'src/templates/index.template.us',
                dest: 'dist/index.html',
                variables: {
                    gae: false,
                    min: true,
                    opts: options
                }
            }
        };

        config.production.variables.gae = options.gae && options.gae !== ""

        return config;
    };

}());
