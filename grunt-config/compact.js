(function() {
    'use strict';

    exports.config = function(options) {

        var config = {
                'development': {
                    'temp/libs.js': [
                        '.cache/lib/modernizr.js',
                        '.cache/lib/jquery.js',
                        '.cache/lib/dust-core.js',
                        '.cache/lib/underscore.js',
                        '.cache/lib/backbone.js',
                        '.cache/lib/backbone.marionette.js',
                        '.cache/lib/backbone.geppetto.js',
                        '.cache/lib/bootstrap.js',
                        '.cache/lib/moment.js',
                        '.cache/lib/es.js',
                        '.cache/lib/require.js'
                    ]
                },
                'production': {
                    'temp/libs.js': [
                        '.cache/lib/modernizr.min.js',
                        '.cache/lib/jquery.min.js',
                        '.cache/lib/underscore-min.js',
                        '.cache/lib/lodash.backbone.min.js',
                        '.cache/lib/backbone-min.js',
                        '.cache/lib/backbone.marionette.min.js',
                        '.cache/lib/backbone.geppetto.min.js',
                        '.cache/lib/bootstrap.min.js',
                        '.cache/lib/moment.min.js',
                        '.cache/lib/es.min.js',
                        '.cache/lib/require.min.js'
                    ]
                }
            };

        return config;
    };

}());