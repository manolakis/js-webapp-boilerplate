(function() {
    'use strict';

    exports.config = function(options) {

        var config = {
            options: {
                src: 'src/**/*.coffee',
                indentation: {
                    value: 3,
                    level: 'error'
                },
                max_line_length: {
                    level: 'ignore'
                }
            }
        };

        return config;
    };

}());