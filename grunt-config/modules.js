(function() {
    'use strict';

    exports.config = function(options) {

        var config = {
            config: {
                modules: options.modules,
                src: 'temp/modules/',
                template: 'src/templates/module.template.us'
            }
        };

        return config;
    };

}());