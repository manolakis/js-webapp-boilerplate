(function() {
    'use strict';

    exports.config = function(options) {

        var config = {
            dev: {
                options: {
                    compress: false
                },
                files: {
                    "dist/css/styles.css": "src/styles/stylus/styles.styl"
                }
            },
            release: {
                options: {
                    compress: true
                },
                files: {
                    "dist/css/styles.min.css": "src/styles/stylus/styles.styl"
                }
            }
        };

        return config;
    };

}());