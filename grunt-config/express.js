(function() {
    'use strict';

    exports.config = function(options) {

        var config = {
            server: {
                options: {
                    script: 'server.js'
                }
            }
        };

        return config;
    };

}());