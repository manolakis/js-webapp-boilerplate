(function() {
    'use strict';

    exports.config = function(options) {

        var config = {
            options: {
                thresholds: {
                    'statements': 90,
                    'branches': 90,
                    'lines': 90,
                    'functions': 90
                },
                dir: 'coverage',
                root: 'test'
            }
        };

        return config;
    };

}());



