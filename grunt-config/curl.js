(function() {
    'use strict';

    exports.config = function(options) {

        var fs = require('fs'),
            fileName = function(name) {
                var index = name.lastIndexOf("/");
                return name.substr(index + 1);
            },
            assets = {
                "css": {
                    "dest": ".cache/css/",
                    "files": [
                        "http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/css/bootstrap.css",
                        "http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/css/bootstrap-responsive.css"
                    ]
                },
                "img": {
                    "dest": ".cache/img/",
                    "files": [
                        "http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/img/glyphicons-halflings-white.png",
                        "http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/img/glyphicons-halflings.png"
                    ]
                },
                "lib": {
                    "dest": ".cache/lib/",
                    "files": [
                        "http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/dustjs-linkedin/1.2.3/dust-core.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/dustjs-linkedin/1.2.3/dust-core.min.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.1/underscore.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.1/underscore-min.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.0.0/backbone.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.0.0/backbone-min.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/backbone.marionette/1.0.4-bundled/backbone.marionette.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/backbone.marionette/1.0.4-bundled/backbone.marionette.min.js",
                        "http://raw.github.com/ModelN/backbone.geppetto/0.6/backbone.geppetto.js",
                        "http://raw.github.com/ModelN/backbone.geppetto/0.6/dist/backbone.geppetto.min.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/js/bootstrap.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/js/bootstrap.min.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.1.0/moment.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.1.0/moment.min.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.0.0/lang/es.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.0.0/lang/es.min.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/require.js/2.1.8/require.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/require.js/2.1.8/require.min.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/require-i18n/2.0.1/i18n.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/require-i18n/2.0.1/i18n.min.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/require-text/2.0.5/text.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/require-text/2.0.5/text.min.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/require-domReady/2.0.1/domReady.js",
                        "http://cdnjs.cloudflare.com/ajax/libs/require-domReady/2.0.1/domReady.min.js"
                    ]
                }
            },
            config = {};

        Object.keys(assets).forEach(function(assetGroup) {
            var group = assets[assetGroup],
                files = [];

            group.files.forEach(function(asset) {
                var file = group.dest + fileName(asset);
                if (!fs.existsSync(file)) {
                    files.push(asset);
                }
            });

            config[group.dest] = {};
            config[group.dest].src = files;
            config[group.dest].dest = group.dest;

        });

        return config;
    };

}());