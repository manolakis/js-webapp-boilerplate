(function() {
    'use strict';

    exports.config = function(options) {

        var config = {
            modules: {
                files: {}
            }
        };

        options.modules.forEach(function(module) {
            config.modules.files['dist/js/' + module + '.js'] = [
                'temp/build/' + module + '-dust-templates.js',
                'temp/build/' + module + '.js'
            ];
        });

        return config;
    };

}());