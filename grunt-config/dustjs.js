(function() {
    'use strict';

    exports.config = function(options) {

        var config = {};

        options.modules.forEach(function(module) {
            var files = {}

            files['temp/build/' + module + '-dust-templates.js'] = ['src/modules/' + module + '/**/*.html'];
            config[module] = { files: files };
        });

        return config;
    };

}());