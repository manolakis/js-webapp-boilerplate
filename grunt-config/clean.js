(function() {
    'use strict';

    exports.config = function(options) {

        var config = {
            temporal: [
                'temp/'
            ],
            distribution: [
                'dist/',
                'junit/*.xml'
            ]
        };

        return config;
    };

}());