(function() {
    'use strict';

    exports.config = function(options) {

        var config = {
            development: {
                options: {
                    appDir: "temp/app",
                    baseUrl: ".",
                    dir: "temp/build",
                    optimize: 'none',
                    paths: {
                        domReady : '../../.cache/lib/domReady',
                        i18n : '../../.cache/lib/i18n',
                        text: '../../.cache/lib/text'
                    },
                    modules: [
                        {
                            name: "main"
                        }
                    ]
                }
            },
            production: {
                options: {
                    appDir: "temp/app",
                    baseUrl: ".",
                    dir: "temp/build",
                    optimize: 'uglify',
                    paths: {
                        domReady : '../../.cache/lib/domReady.min',
                        i18n : '../../.cache/lib/i18n.min',
                        text: '../../.cache/lib/text.min'
                    },
                    modules: [
                        {
                            name: "main"
                        }
                    ]
                }
            }
        };

        options.modules.forEach(function(module) {
            config.development.options.modules.push({
                name: module,
                exclude: ['main']
            });
            config.production.options.modules.push({
                name: module,
                exclude: ['main']
            });
        });

        return config;
    };

}());



