(function() {
    'use strict';

    exports.config = function(options) {

        var config = {
            prepare: {
                files: [
                    {
                        expand: true,
                        cwd: 'src/app/',
                        src: '**/*',
                        dest: 'temp/app/'
                    },
                    {
                        expand: true,
                        cwd: 'src/modules/',
                        src: '**/*',
                        dest: 'temp/modules/'
                    }
                ]
            },
            "pre-require": {
                files: [
                    {
                        expand: true,
                        cwd: 'temp/modules/',
                        src: '**/*',
                        dest: 'temp/app/'
                    }
                ]
            },
            styles: {
                files: [
                    {
                        expand: true,
                        cwd: 'src/styles/',
                        src: '**/*',
                        dest: 'temp/styles/'
                    },
                    {
                        expand: true,
                        cwd: '.cache/css',
                        src: '**/*',
                        dest: 'temp/styles/css'
                    }
                ]
            }
        };

        return config;
    };

}());