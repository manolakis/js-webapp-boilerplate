(function() {
    'use strict';

    exports.config = function(options) {
        var config = {
            development: {
                files: 'src/**/*',
                tasks: 'distribute-for-development',
                options: {
                    nospawn: true
                }
            },
            production: {
                files: 'src/**/*',
                tasks: 'distribute-for-production',
                options: {
                    nospawn: true
                }
            }
        };

        return config;
    };

}());