module.exports = function(grunt) {
    'use strict';

    var options = grunt.file.readJSON('package.json'),
        fs = require('fs'),
        packages = [];

    fs.readdirSync('src/modules').forEach(function(file) {
        var stats = fs.statSync('src/modules/' + file);
        if (stats.isDirectory()) {
            packages.push(file);
        }
    });

    options['modules'] = packages;

    grunt.initConfig({
        'curl-dir': require('./grunt-config/curl.js').config(options),
        compact : require('./grunt-config/compact.js').config(options),
        shafile : require('./grunt-config/shafile.js').config(options),
        moduleHasher : {
            modules: packages
        },
        clean: require('./grunt-config/clean.js').config(options),
        copy: require('./grunt-config/copy.js').config(options),
        concat: require('./grunt-config/concat.js').config(options),
        coffeelint: require('./grunt-config/coffeelint.js').config(options),
        coffee: require('./grunt-config/coffee.js').config(options),
        dustjs: require('./grunt-config/dustjs.js').config(options),
        watch: require('./grunt-config/watch.js').config(options),
        less: require('./grunt-config/less.js').config(options),
        stylus: require('./grunt-config/stylus.js').config(options),
        requirejs: require('./grunt-config/requirejs.js').config(options),
        modules: require('./grunt-config/modules.js').config(options),
        modernizr: require('./grunt-config/modernizr.js').config(options),
        template: require('./grunt-config/template.js').config(options),
        coverage: require('./grunt-config/coverage.js').config(options),
        express: require('./grunt-config/express.js').config(options)
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-coffee');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-istanbul-coverage');
    grunt.loadNpmTasks('grunt-curl');
    grunt.loadNpmTasks('grunt-modernizr');
    grunt.loadNpmTasks('grunt-coffeelint');
    grunt.loadNpmTasks('grunt-dustjs');
    grunt.loadNpmTasks('grunt-templater')
    grunt.loadNpmTasks('grunt-express-server');

    // - tasks definition -------------------------------------------

    require('./grunt-tasks/modules.js').task(grunt);
    require('./grunt-tasks/compact.js').task(grunt);
    require('./grunt-tasks/shafile.js').task(grunt);
    require('./grunt-tasks/moduleHasher.js').task(grunt);

    grunt.registerTask('clean-distribution', ['clean:distribution']);
    grunt.registerTask('clean-temporal-files', ['clean:temporal']);

    grunt.registerTask('obtain-static-assets', 'Obtains all external static assets', function(){
        var fs = require('fs'),
            tasks = ['curl-dir'];

        if (!fs.existsSync('.cache/lib/modernizr.min.js')) {
            tasks.push('modernizr');
        }

        grunt.task.run(tasks);
    });

    grunt.registerTask('compile-js-for-development', [
        'obtain-static-assets',
        'compact:development',
        'shafile:development-libs',
        'coffeelint',
        'copy:prepare',
        'coffee',
        'modules',
        'copy:pre-require',
        'requirejs:development',
        'dustjs',
        'moduleHasher',
        'shafile:development-main',
        'clean-temporal-files'
    ]);

    grunt.registerTask('compile-js-for-production', [
        'obtain-static-assets',
        'compact:production',
        'shafile:production-libs',
        'coffeelint',
        'copy:prepare',
        'coffee',
        'modules',
        'copy:pre-require',
        'requirejs:production',
        'dustjs',
        'moduleHasher',
        'shafile:production-main',
        'clean-temporal-files'
    ]);

    grunt.registerTask('compile-css-for-development', [
        'obtain-static-assets',
        'copy:styles',
        'less:development',
        'shafile:development-css',
        'template:development'
    ]);

    grunt.registerTask('compile-css-for-production', [
        'obtain-static-assets',
        'copy:styles',
        'less:production',
        'shafile:production-css',
        'template:production'
    ]);

    grunt.registerTask('distribute-for-development', [
        'clean-distribution',
        'compile-js-for-development',
        'compile-css-for-development',

    ]);

    grunt.registerTask('distribute-for-production', [
        'clean-distribution',
        'compile-js-for-production',
        'compile-css-for-production'
    ]);


    grunt.registerTask('server:dev', [
        'distribute-for-development',
        'express',
        'watch:development'
    ]);

    grunt.registerTask('server:prod', [
        'distribute-for-production',
        'express',
        'watch:production'
    ]);

    grunt.registerTask('default', ['server:dev']);

};

