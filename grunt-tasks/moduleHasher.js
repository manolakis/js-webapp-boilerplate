(function() {
    'use strict';

    exports.task = function(grunt) {
        grunt.registerMultiTask('moduleHasher', '', function() {
            var fs = require('fs'),
                path = require('path'),
                crypto = require('crypto'),
                modules = {},
                contents = '';

            fs.mkdirParentSync = fs.mkdirParentSync || function(dirPath) {
                try {
                    if (!fs.existsSync(dirPath)) {
                        fs.mkdirSync(dirPath);
                    }
                } catch (e) {
                    fs.mkdirParentSync(path.dirname(dirPath));
                    fs.mkdirParentSync(dirPath);
                }
            };

            this.data.forEach(function(module) {
                var shasum = crypto.createHash('sha1'),
                    shahex,
                    libs = [],
                    name;

                name = 'temp/build/' + module + '-dust-templates.js';
                if (fs.existsSync(name)) {
                    libs.push(name);
                    var content = fs.readFileSync(name);
                    contents += content + ';';
                    shasum.update(content);
                }

                name = 'temp/build/' + module + '.js';
                if (fs.existsSync(name)) {
                    libs.push(name);
                    var content = fs.readFileSync(name);
                    contents += content + ';';
                    shasum.update(content);
                }

                shahex = shasum.digest('hex');

                name = 'js/' + module + '-' + shahex + '.js';

                modules[module] = name;

                fs.mkdirParentSync(path.dirname('dist/' + name));
                fs.writeFileSync('dist/' + name, contents);
            });

            console.log(modules);

            fs.mkdirParentSync(path.dirname('dist/modules.json'));
            fs.writeFileSync('dist/modules.json', JSON.stringify(modules));
        });
    }

}());