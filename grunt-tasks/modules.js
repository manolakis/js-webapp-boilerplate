(function() {
    'use strict';

    exports.task = function(grunt) {
        grunt.registerMultiTask('modules', '', function() {
            // TODO default parameters
            var srcDir = /\/$/.test(this.data.src) ? this.data.src : this.data.src + '/',
                _ = require('underscore'),
                fs = require('fs'),
                template = grunt.file.read(this.data.template);


            this.data.modules.forEach(function(module) {
                var files = [];
                (function(src) {
                    var pattern = /(.*).js/i,
                        findFiles = function(source) {
                            fs.readdirSync(source).forEach(function(file) {
                                var stats = fs.statSync(source + file);
                                if (stats.isDirectory()) {
                                    findFiles(source + file + '/');
                                } else if (stats.isFile() && pattern.test(file)) {
                                    var matches = file.match(pattern);
                                    files.push(source.substr(srcDir.length) + matches[1]);
                                }
                            });
                        };
                    findFiles(src);
                })(srcDir + module + '/');

                var compiled = _.template(template, {files: files});

                grunt.file.write(srcDir + module + '.js', compiled);
            });
        });
    }

}());