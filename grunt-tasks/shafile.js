(function() {
    'use strict';

    exports.task = function(grunt) {
        grunt.registerMultiTask('shafile', '', function() {

            grunt.config.requires(
                'shafile.' + this.target + '.src',
                'shafile.' + this.target + '.dest'
            );

            var fs = require('fs'),
                path = require('path'),
                crypto = require('crypto'),
                algorithm = this.data.algorithm || 'sha1',
                shasum = crypto.createHash(algorithm),
                src = this.data.src,
                dest = this.data.dest,
                pattern = /\{sha\}/g,
                shortName = function(name) {
                    var index = name.lastIndexOf('/');
                    return name.substr(index + 1);
                };

            fs.mkdirParentSync = fs.mkdirParentSync || function(dirPath) {
                try {
                    if (!fs.existsSync(dirPath)) {
                        fs.mkdirSync(dirPath);
                    }
                } catch (e) {
                    fs.mkdirParentSync(path.dirname(dirPath));
                    fs.mkdirParentSync(dirPath);
                }
            };

            if (fs.existsSync(src)) {
                if (pattern.test(dest)) {
                    var content = fs.readFileSync(src),
                        shahex,
                        name;

                    shasum.update(content);
                    shahex = shasum.digest('hex');

                    name = dest.replace('{sha}', shahex);

                    fs.mkdirParentSync(path.dirname(name));
                    fs.writeFileSync(name, content);

                    if (this.data.env) {
                        grunt.config(this.data.env, shortName(name));
                    }
                } else {
                    grunt.fail.fatal('dest don\'t have {sha} pattern');
                }
            } else {
                grunt.fail.fatal('src ' + src + ' file not found');
            }
        });
    }

}());