(function() {
    'use strict';

    exports.task = function(grunt) {
        grunt.registerMultiTask('compact', '', function() {
            var self = this;

            Object.keys(this.data).forEach(function(dest){
                var fs = require('fs'),
                    path = require('path'),
                    files = self.data[dest],
                    separator = self.data.separator || ';\n',
                    contents = '';

                fs.mkdirParentSync = fs.mkdirParentSync || function(dirPath) {
                    try {
                        if (!fs.existsSync(dirPath)){
                            fs.mkdirSync(dirPath);
                        }
                    } catch(e) {
                        fs.mkdirParentSync(path.dirname(dirPath));
                        fs.mkdirParentSync(dirPath);
                    }
                };

                files.forEach(function(fileName) {
                    if (fs.existsSync(fileName)) {
                        var content = fs.readFileSync(fileName) + separator;
                        contents += content;
                    }
                });

                fs.mkdirParentSync(path.dirname(dest));
                fs.writeFileSync(dest, contents);
            });
        });
    }

}());