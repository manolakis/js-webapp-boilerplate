(function() {
    var _ref, _ref1,
        express = require('express'),
        dir = "" + __dirname + "/dist",
        port = (_ref = (_ref1 = process.env.PORT) != null ? _ref1 : process.argv.splice(2)[0]) != null ? _ref : 8080,
        app = express();

    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.errorHandler());
    app.use(express["static"](dir));
    app.use(app.router);

    app.listen(port, function() {
        return console.log("started web server at http://localhost:" + port);
    });

}).call(this);